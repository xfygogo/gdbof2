# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2021-04-21)


### Bug Fixes

* nEEDFIX: gdbOFexpanded.gdb中的pSurfaceValues有待分析和修改 ([52ecc8c](///commit/52ecc8cbed699d581c82446c032f36ab00062f9d))
* 修改原始仓库代码中的部分bug，使大部分功能能正常使用 ([70e44fe](///commit/70e44fe31535c7e18357a0c184b1d15905594f72))
